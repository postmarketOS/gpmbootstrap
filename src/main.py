
import sys
import gi

gi.require_version("Gtk", "4.0")
gi.require_version("Adw", "1")

from gi.repository import Gio, Adw
from .window import GpmbootstrapWindow
from .preferences import GpmbootstrapPreferences

class GpmbootstrapApplication(Adw.Application):

    def __init__(self):
        super().__init__(application_id="org.postmarketos.gpmbootstrap",
                         flags=Gio.ApplicationFlags.DEFAULT_FLAGS)
        self.create_action("quit", lambda *_: self.quit(), ["<primary>q"])
        self.create_action("about", self.on_about_action)
        self.create_action("preferences", self.on_preferences_action)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = GpmbootstrapWindow(application=self)
        win.present()
        self.prefs = GpmbootstrapPreferences()

    def on_about_action(self, widget, _):
        about = Adw.AboutWindow(transient_for=self.props.active_window,
                                application_name="gpmbootstrap",
                                application_icon="org.postmarketos.gpmbootstrap",
                                version=self.version,
                                issue_url="https://gitlab.com/postmarketOS/gpmbootstrap/-/issues/new",
                                )
        about.present()

    def on_preferences_action(self, widget, _):
        self.prefs.set_transient_for(self.props.active_window)
        self.prefs.present()

    def create_action(self, name, callback, shortcuts=None):
        # Adds an action.

        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)


def main(version):
    app = GpmbootstrapApplication()
    app.version = version
    return app.run(sys.argv)
