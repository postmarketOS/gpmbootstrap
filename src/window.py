
from gi.repository import Adw
from gi.repository import Gtk
from gi.repository import GLib

from .utils import pmos
from .utils import image

from threading import Thread
import sys

@Gtk.Template(resource_path='/org/postmarketos/gpmbootstrap/ui/window.ui')
class GpmbootstrapWindow(Adw.ApplicationWindow):
    __gtype_name__ = 'GpmbootstrapWindow'

    carousel = Gtk.Template.Child()

    device_search_bar = Gtk.Template.Child()
    device_search_entry = Gtk.Template.Child()

    search_button = Gtk.Template.Child()
    menu_button = Gtk.Template.Child()

    device_list = Gtk.Template.Child()
    ui_list = Gtk.Template.Child()
    kernel_list = Gtk.Template.Child()
    flash_list = Gtk.Template.Child()

    fde_switch = Gtk.Template.Child()
    user_name_entry = Gtk.Template.Child()
    password_entry = Gtk.Template.Child()
    hostname_entry = Gtk.Template.Child()

    install_button = Gtk.Template.Child()

    can_flash = False

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.device_list.connect("map", self.load_devices)
        self.device_list.connect("selected_rows_changed", self.device_selected)
        self.device_list.set_filter_func(self.search_devices)
        self.ui_list.connect("selected_rows_changed", self.ui_selected)
        self.kernel_list.connect("selected_rows_changed", self.kernel_selected)
        self.carousel.connect("page_changed", self.carousel_page_changed)
        self.fde_switch.connect("state_set", self.change_fde)
        self.install_button.connect("clicked", self.install)
        self.device_search_entry.connect("search_changed", self.invalidate_device_filter)
        self.search_button.bind_property("active", self.device_search_bar, "search-mode-enabled")
        pmos.init()

    def carousel_page_changed(self, user_data, index):
        # Search and preferences are only supported on the first (select device) page
        if index >= 1:
            self.search_button.set_sensitive(0)
            self.menu_button.set_sensitive(0)
        else:
            self.search_button.set_sensitive(1)
            self.menu_button.set_sensitive(1)

    def invalidate_device_filter(self, user_data):
        self.device_list.invalidate_filter()

    def search_devices(self, user_data):
        search = self.device_search_entry.get_text().lower()
        return search in user_data.get_title().lower() or search in user_data.get_subtitle()

    def device_selected(self, user_data):
        # Scroll to the kernel select page
        self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 1), True)
        device_name = user_data.get_selected_row().get_subtitle()
        pmos.set_device(device_name)
        kernels = pmos.get_kernels()
        # Scroll to the ui select page if only one kernel is available
        if kernels is None:
            self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 2), True)
        else:
            for kernel in kernels:
                ar = Adw.ActionRow()
                ar.set_title(kernel)
                ar.set_subtitle(kernels[kernel])
                self.kernel_list.append(ar)
        # Load uis and add them to the ui select page
        uis = pmos.get_uis(pmos)
        for ui in uis:
            ar = Adw.ActionRow()
            ar.set_use_markup(False)
            ar.set_title(ui[0])
            ar.set_subtitle(ui[1])
            self.ui_list.append(ar)

    def load_devices(self, user_data):
        # Empty the list.
        while self.device_list.get_first_child() is not None:
            self.device_list.remove(self.device_list.get_first_child())
        devices = pmos.get_devices()
        for device in devices:
            ar = Adw.ActionRow()
            ar.set_use_markup(False)
            ar.set_subtitle(next(iter(device)))
            ar.set_title(device[next(iter(device))])
            self.device_list.append(ar)

    def kernel_selected(self, user_data):
        # Scroll to the ui select page when a kernel is selected

        self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 1), True)
        pmos.set_kernel(user_data.get_selected_row().get_title())

    def ui_selected(self, user_data):
        self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 1), True)
        pmos.set_ui(user_data.get_selected_row().get_title())
        if image.get_image_downloadable(pmos.argssubstitute.device, pmos.get_current_channel(), pmos.argssubstitute.ui):
            dialog = Adw.MessageDialog(heading="Pre-built image available", body="Would you like to download and flash a pre-built image instead?")
            dialog.add_response("no", "No")
            dialog.add_response("yes", "Yes")
            dialog.set_transient_for(self)
            dialog.connect("response", self.prebuilt_image)
            dialog.show()

    def change_fde(self, user_data, state):
        pmos.installargssubstitute.full_disk_encryption = state
        if state:
            dialog = Adw.MessageDialog(heading="WARNING", body="Full-disk encryption is not supported on all devices and it may cause flashing to fail.")
            dialog.add_response("Continue", "Continue")
            dialog.set_transient_for(self)
            dialog.show()

    def prebuilt_image(self, user_data, response):
        if response == "yes":
            self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 1), True)
            self.install_dialog = Adw.MessageDialog(heading="Downloading postmarketos...", body="This might take a few minutes.")
            self.install_dialog.set_transient_for(self)
            self.install_dialog.show()
            self.install_thread = Thread(target=pmos.download_images, args=[self], daemon=True)
            self.install_thread.start()

    def install(self, user_data):
        # Scroll to the flashing page and start installing postmarketos

        pmos.installargssubstitute.user = self.user_name_entry.get_text()
        pmos.installargssubstitute.password = self.password_entry.get_text()
        pmos.installargssubstitute.hostname = self.hostname_entry.get_text()

        if pmos.installargssubstitute.user == "" or pmos.installargssubstitute.password == "" or pmos.installargssubstitute.hostname == "":
            dialog = Adw.MessageDialog()
            dialog.set_heading("Error")
            dialog.set_body("Provide a password, username and hostname to install postmarketos.")
            dialog.add_response("cancel", "Ok")
            dialog.set_transient_for(self)
            dialog.show()
            return

        self.carousel.scroll_to(self.carousel.get_nth_page(self.carousel.get_position() + 1), True)
        self.install_dialog = Adw.MessageDialog(heading="Installing postmarketos...", body="This might take a few minutes.")
        self.install_dialog.set_transient_for(self)
        self.install_dialog.show()
        # Setup arguments for pmbootstrap install.
        pmos.installargssubstitute.android_recovery_zip = self.get_application().prefs.recovery_zip_switch.get_active()
        pmos.installargssubstitute.on_device_installer = self.get_application().prefs.ondev_switch.get_active()
        # Run pmbootstrap install in another thread.
        self.install_thread = Thread(target=pmos.install, args=[self], daemon=True)
        self.install_thread.start()

    def continue_flash(self, user_data, response):
        self.can_flash = True

    def exit_connect(self, user_data, response):
        sys.exit()

    def show_connect_device_dialog(self, mode):
        self.can_flash = False
        dialog = Adw.MessageDialog()
        dialog.set_heading("Connect a device")
        dialog.add_response("cancel",  "Continue")
        dialog.set_transient_for(self)
        dialog.connect("response", self.continue_flash)
        if mode != "none":
           dialog.set_body(f"Connect a device in {mode} mode.")
        else:
           dialog.set_body("Connect a device to flash postmarketos on.")
        GLib.idle_add(dialog.choose)

    def show_flashing_not_supported_dialog(self):
        self.can_flash = False
        dialog = Adw.MessageDialog()
        dialog.set_heading("Flashing not supported")
        dialog.set_body("Flashing to this device is not supported. Try using pmbootstrap flasher.")
        dialog.add_response("cancel", "Exit")
        dialog.set_transient_for(self)
        dialog.connect("response", self.exit_connect)
        GLib.idle_add(dialog.choose)

    def start_flashing(self):
        # Install of postmarketos is complete, start flashing

        self.install_dialog.destroy()
        if self.get_application().prefs.recovery_zip_switch.get_active():
            pmos.argssubstitute.flash_method = "adb"
            pmos.argssubstitute.deviceinfo["flash_method"] = "adb"
        if pmos.get_flash_method() == "none":
            self.show_flashing_not_supported_dialog()
            return
        flash_actions = pmos.get_flash_actions()
        # Add flash actions to the flashing page.
        for action in flash_actions:
            ar = Adw.ActionRow()
            ar.set_use_markup(False)
            ar.set_title(action)
            self.flash_list.append(ar)
        # Run flash actions.
        for i, action in enumerate(flash_actions):
            spinner = Gtk.Spinner()
            ar = self.flash_list.get_row_at_index(i)
            ar.add_suffix(spinner)
            connected = pmos.is_device_connected()
            if pmos.get_flash_method() == "heimdall-bootimg" and self.get_application().prefs.reboot_switch.get_active():
                if len(flash_actions) > 1:
                    pmos.argssubstitute.no_reboot = True
                if len(flash_actions) > 1 and i >= 1:
                    pmos.argssubstitute.resume = True
            if not connected:
                if connected is None:
                    self.show_flashing_not_supported_dialog()
                else:
                    self.show_connect_device_dialog(pmos.get_flash_method())
                while not self.can_flash:
                    pass
            spinner.start()
            pmos.flash(action)
            spinner.set_visible(False)
            check = Gtk.Image().new_from_icon_name("emblem-ok-symbolic")
            ar.add_suffix(check)
        done(self)

def done(self):
    dialog = Adw.MessageDialog()
    dialog.set_heading("DONE")
    dialog.set_body("Postmarketos is now installed on your device.")
    dialog.add_response("cancel", "Exit")
    dialog.set_transient_for(self)
    dialog.connect("response", self.exit_connect)
    GLib.idle_add(dialog.choose)
