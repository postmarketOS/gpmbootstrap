# gpmbootstrap

GTK frontend for pmbootstrap.

## ⚠️ This code is an experiment!

If you want to install postmarketOS, read the
[installation](https://wiki.postmarketos.org/wiki/Installation) article first
to decide whether you want to install from an image or with pmbootstrap.

If you want to install from an image, you don't need this tool. If you want to
install from pmbootstrap, you are highly encouraged to use
[pmbootstrap directly](https://wiki.postmarketos.org/wiki/Pmbootstrap) instead
of gpmbootstrap. The gpmbootstrap code base is an experiment at this point, it
may not work as expected. Unlike pmbootstrap, the code is not tried and tested
and has not been reviewed at all as of writing!

## Building

```
meson build
ninja -C build install
```

## Requirements

 * python
 * pmbootstrap
 * gtk 4.0+
 * Adwaita
 * polkit
