from gpmbootstrap import utils

class window():
    def start_flashing():
        pass

def init():
    utils.pmos.init("sudo")

def test_install():
    utils.pmos.set_device("pine64-pinephone")
    utils.pmos.set_ui("none")
    utils.pmos.installargssubstitute.user = "user"
    utils.pmos.installargssubstitute.password = "147147"
    utils.pmos.installargssubstitute.hostname = "gpmb-test"
    utils.pmos.installargssubstitute.no_image = True
    utils.pmos.install(window)

init()