from gpmbootstrap import utils

class window():
    def start_flashing():
        pass

def init():
    utils.pmos.init("sudo")

def test_images():
    assert utils.image.get_image_downloadable("pine64-pinephone", "edge", "sxmo-de-sway") is True
    utils.image.download_images("pine64-pinephone", "edge", "sxmo-de-sway", utils.pmos.argssubstitute)

init()