#!/bin/sh -e
# Description: run pmbootstrap python testsuite
# Options: native slow
# https://postmarketos.org/pmb-ci

if [ "$(id -u)" = 0 ]; then
	exec su "${TESTUSER:-build}" -c "sh -e $0"
fi

# Require pytest to be installed on the host system
if [ -z "$(command -v pytest)" ]; then
	echo "ERROR: pytest command not found, make sure it is in your PATH."
	exit 1
fi

echo "Running pytest..."
pytest \
	--color=yes \
	-vv \
	-x \
	test \
		-m "not skip_ci" \
		"$@"
